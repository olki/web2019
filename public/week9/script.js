function calcSum() {
    let a = document.getElementById('inputA').value;
    let b = document.getElementById('inputB').value;

    let c = parseInt(a) + parseInt(b);
    document.getElementById('answer').value = c;
}

let a = 0;
function changeClass() {
    if(a % 3 == 0) {
        document.getElementById('box').className = 'col col-lg-3 col-xs-3 bg-danger box';
    } else if (a % 3 == 1) {
        document.getElementById('box').className = 'col col-lg-3 col-xs-3 bg-primary box';
    } else {
        document.getElementById('box').className = 'col col-lg-3 col-xs-3 bg-success box';
    }

    a = a + 1;
}